﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlComida.Entity.Data;

namespace ControlComida.Library.Helpers
{
    public static class SolicitudDetHelper
    {
        public static Model.mSolicitudDet toModel(this tbl_Solicitud_Comida_Det table)
        {
            var model = new Model.mSolicitudDet
            {
                NumeroSolicitud = table.No_Solicitud,
                TipoComida = table.Tipo_Comida,
                AreadeSolicitud = table.AreaSolicitud,
                CodigoEmpleado = table.Codigo_Empleado,
                HorarioTrabajo = table.Horario_Trabajo
            };

            return model;
        }

        public static tbl_Solicitud_Comida_Det toTable(this Model.mSolicitudDet SolicitudDet)
        {
            var model = new tbl_Solicitud_Comida_Det
            {
                No_Solicitud = SolicitudDet.NumeroSolicitud,
                Tipo_Comida = SolicitudDet.TipoComida,
                AreaSolicitud = SolicitudDet.AreadeSolicitud,
                Codigo_Empleado = SolicitudDet.CodigoEmpleado,
                Horario_Trabajo = SolicitudDet.HorarioTrabajo
            };

            return model;
        }

        public static List<Model.mSolicitudDet> GetAllSolicitudesDet()
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                List<Model.mSolicitudDet> SolicitudDet = new List<Model.mSolicitudDet>();
                var result = context.tbl_Solicitud_Comida_Det;
                if (result == null)
                {
                    return null;
                }

                foreach (var item in result)
                {
                    SolicitudDet.Add(item.toModel());
                }

                return SolicitudDet;
            }
        }

        public static void SetAllSolicitudesDet(Model.mSolicitudDet vSolicitudDet)
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                try
                {
                    context.tbl_Solicitud_Comida_Det.Add(vSolicitudDet.toTable());
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
            }
        }
    }
}
