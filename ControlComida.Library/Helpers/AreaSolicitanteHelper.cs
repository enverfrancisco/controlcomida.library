﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlComida.Entity.Data;

namespace ControlComida.Library.Helpers
{
    public static class AreaSolicitanteHelper
    {
        public static Model.mAreaSolicitante toModel(this AreaSolicitante table)
        {
            var model = new Model.mAreaSolicitante
            {
                IdArea = table.Id,
                DesccripcionArea = table.DescArea
            };

            return model;
        }

        public static AreaSolicitante toTable(this Model.mAreaSolicitante area)
        {
            var model = new AreaSolicitante
            {
                Id = area.IdArea,
                DescArea = area.DesccripcionArea
            };

            return model;
        }

        public static List<Model.mAreaSolicitante> GetAllArea()
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                List<Model.mAreaSolicitante> Areas = new List<Model.mAreaSolicitante>();
                var result = context.AreaSolicitante;
                if (result == null)
                {
                    return null;
                }

                foreach (var item in result)
                {
                    Areas.Add(item.toModel());
                }

                return Areas;
            }
        }

        public static void SetAllAreas(Model.mAreaSolicitante vAreas)
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                try
                {
                    context.AreaSolicitante.Add(vAreas.toTable());
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
            }
        }
    }
}
