﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlComida.Entity.Data;

namespace ControlComida.Library.Helpers
{
    public static class SolicitudEncHelper
    {
        public static Model.mSolicitudEnc toModelEnc(this tbl_Solicitud_Comida_Enc table)
        {
            var model = new Model.mSolicitudEnc
            {
                Numero_Solicitud = table.No_Solicitud,
                TipoComida = table.Tipo_Comida,
                AreadeSolicitud = table.AreaSolicitud,
                IdComedor = table.Comedor,
                Observacion = table.Observaciones,
                UsuarioAgrega = table.Usuario_Agrega,
                UsuarioAutoriza = table.Usuario_Autoriza,
                UsuarioVerifica = table.Usuario_Verifica,
                FechaIngreso = table.Fecha_Ingreso,
                FechaSolicitud = table.Fecha_Solicitud
            };

            return model;
        }

        public static Model.mSolicitudDet toModelDet(this tbl_Solicitud_Comida_Det table)
        {
            var model = new Model.mSolicitudDet
            {
                NumeroSolicitud = table.No_Solicitud,
                TipoComida = table.Tipo_Comida,
                AreadeSolicitud = table.AreaSolicitud,
                CodigoEmpleado = table.Codigo_Empleado,
                HorarioTrabajo = table.Horario_Trabajo
            };

            return model;
        }

        public static tbl_Solicitud_Comida_Enc toTableEnc(this Model.mSolicitudEnc SolicitudEnc)
        {

            //Model.mSolicitudEnc dbDetalle = new Model.mSolicitudEnc();

            var table = new tbl_Solicitud_Comida_Enc
            {
                No_Solicitud = SolicitudEnc.Numero_Solicitud,
                Tipo_Comida = SolicitudEnc.TipoComida,
                AreaSolicitud = SolicitudEnc.AreadeSolicitud,
                Comedor = SolicitudEnc.IdComedor,
                Observaciones = SolicitudEnc.Observacion,
                Usuario_Agrega = SolicitudEnc.UsuarioAgrega,
                Usuario_Autoriza = SolicitudEnc.UsuarioAutoriza,
                Usuario_Verifica = SolicitudEnc.UsuarioVerifica,
                Fecha_Ingreso = SolicitudEnc.FechaIngreso,
                Fecha_Solicitud = SolicitudEnc.FechaSolicitud                
            };

            if (SolicitudEnc.Solicitud_ComidaDet != null)
            {
                if (SolicitudEnc.Solicitud_ComidaDet.Count != 0)
                {
                    foreach(var Detalle in SolicitudEnc.Solicitud_ComidaDet)
                    {
                        SolicitudEnc.Solicitud_ComidaDet.Add(Detalle);
                    }
                }
            }

            return table;
        }

        public static tbl_Solicitud_Comida_Det toTableDet(this Model.mSolicitudDet SolicitudDet)
        {
            var table = new tbl_Solicitud_Comida_Det
            {
                No_Solicitud = SolicitudDet.NumeroSolicitud,
                Tipo_Comida = SolicitudDet.TipoComida,
                AreaSolicitud = SolicitudDet.AreadeSolicitud,
                Codigo_Empleado = SolicitudDet.CodigoEmpleado,
                Horario_Trabajo = SolicitudDet.HorarioTrabajo                
            };

            return table;
        }

        public static List<Model.mSolicitudEnc> GetAllSolicitudesEnc()
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                List<Model.mSolicitudEnc> SolicitudEnc = new List<Model.mSolicitudEnc>();
                var result = context.tbl_Solicitud_Comida_Enc;
                if (result == null)
                {
                    return null;
                }

                foreach (var item in result)
                {
                    SolicitudEnc.Add(item.toModelEnc());
                }

                return SolicitudEnc;
            }
        }

        public static void SetAllSolicitudesEnc(Model.mSolicitudEnc vSolicitudEnc)
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                try
                {
                    context.tbl_Solicitud_Comida_Enc.Add(vSolicitudEnc.toTableEnc());

                    context.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {

                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("bla blah",eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        //eve.Entry.Entity.GetType().Name, eve.Entry.State);

                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    
                    e.Message.ToString();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
            }
        }
    }
}
