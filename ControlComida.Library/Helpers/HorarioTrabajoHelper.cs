﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlComida.Entity.Data;

namespace ControlComida.Library.Helpers
{
    public static class HorarioTrabajoHelper
    {
        public static Model.mHorarioTrabajo toModel(this tblHorariosTrabajos table)
        {
            var model = new Model.mHorarioTrabajo
            {
                CodigoHorTrabajo = table.Codigo_Hor_Trabajo,
                DescripcionHorTrabajo = table.Descripcion_Hor_Trabajo
            };

            return model;
        }

        public static tblHorariosTrabajos toTable(this Model.mHorarioTrabajo Trabajo)
        {
            var model = new tblHorariosTrabajos
            {
                Codigo_Hor_Trabajo = Trabajo.CodigoHorTrabajo,
                Descripcion_Hor_Trabajo = Trabajo.DescripcionHorTrabajo
            };

            return model;
        }

        public static List<Model.mHorarioTrabajo> GetAllHorariosTrabajo()
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                List<Model.mHorarioTrabajo> Horarios = new List<Model.mHorarioTrabajo>();
                var result = context.tblHorariosTrabajos;
                if (result == null)
                {
                    return null;
                }

                foreach (var item in result)
                {
                    Horarios.Add(item.toModel());
                }

                return Horarios;
            }
        }

        public static void SetAllHorariosTrabajo(Model.mHorarioTrabajo vTrabajo)
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                try
                {
                    context.tblHorariosTrabajos.Add(vTrabajo.toTable());
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
            }
        }
    }
}
