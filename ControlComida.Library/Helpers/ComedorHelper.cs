﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlComida.Entity.Data;

namespace ControlComida.Library.Helpers
{
    public static class ComedorHelper
    {
        public static Model.mComedor toModel(this Comedor table)
        {
            var model = new Model.mComedor
            {
                IdComedor = table.Id,
                DescripcionComedor = table.DescComedor
            };

            return model;
        }

        public static Comedor toTable(this Model.mComedor Comedor)
        {
            var model = new Comedor
            {
                Id = Comedor.IdComedor,
                DescComedor = Comedor.DescripcionComedor
            };

            return model;
        }

        public static List<Model.mComedor> GetAllComedores()
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                List<Model.mComedor> Comedor = new List<Model.mComedor>();
                var result = context.Comedor;
                if (result == null)
                {
                    return null;
                }

                foreach (var item in result)
                {
                    Comedor.Add(item.toModel());
                }

                return Comedor;
            }
        }

        public static void SetAllComedores(Model.mComedor vComedor)
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                try
                {
                    context.Comedor.Add(vComedor.toTable());
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
            }
        }
    }
}
