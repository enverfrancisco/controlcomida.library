﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlComida.Entity.Data;

namespace ControlComida.Library.Helpers
{
    public static class VistaSolicitudHelper
    {
        public static Model.mVistaSolicitudes toModel(this v_Solicitudes table)
        {
            var model = new Model.mVistaSolicitudes
            {
                NumeroSolicitud = table.No_Solicitud,
                TipoComida = table.Tipo_Comida,
                AreadeSolicitud = table.AreaSolicitud,
                IdComedor = table.Comedor,
                Observacion = table.Observaciones,
                UsuarioAgrega = table.Usuario_Agrega,
                UsuarioAutoriza = table.Usuario_Autoriza,
                UsuarioVerifica = table.Usuario_Verifica,
                FechaIngreso = table.Fecha_Ingreso,
                FechaSolicitud = table.Fecha_Solicitud,
                HorarioTrabajo = table.Horario_Trabajo,
                CodigoEmpleado = table.Codigo_Empleado

            };

            return model;
        }

        public static v_Solicitudes toTable(this Model.mVistaSolicitudes Solicitud)
        {
            var model = new v_Solicitudes
            {
                No_Solicitud = Solicitud.NumeroSolicitud,
                Tipo_Comida = Solicitud.TipoComida,
                AreaSolicitud = Solicitud.AreadeSolicitud,
                Comedor = Solicitud.IdComedor,
                Observaciones = Solicitud.Observacion,
                Usuario_Agrega = Solicitud.UsuarioAgrega,
                Usuario_Autoriza = Solicitud.UsuarioAutoriza,
                Usuario_Verifica = Solicitud.UsuarioVerifica,
                Fecha_Ingreso = Solicitud.FechaIngreso,
                Fecha_Solicitud = Solicitud.FechaSolicitud,
                Horario_Trabajo = Solicitud.HorarioTrabajo,
                Codigo_Empleado = Solicitud.CodigoEmpleado

            };

            return model;
        }

        public static List<Model.mVistaSolicitudes> GetAllVistaSolicitudes()
        {
            using (var context = new ControlComida.Entity.DbContext())
            {
                List<Model.mVistaSolicitudes> Solicitud = new List<Model.mVistaSolicitudes>();
                var result = context.v_Solicitudes;
                if (result == null)
                {
                    return null;
                }

                foreach (var item in result)
                {
                    Solicitud.Add(item.toModel());
                }

                return Solicitud;
            }
        }

        //public static void SetAllVistaSolicitudes(Model.mVistaSolicitudes vVistaSolicitudes)
        //{
        //    using (var context = new Entity.DbContext())
        //    {
        //        try
        //        {
        //            context.v_Solicitudes.Add(vVistaSolicitudes.toTable());
        //            context.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            ex.Message.ToString();
        //        }
        //    }
        //}
    }
}
