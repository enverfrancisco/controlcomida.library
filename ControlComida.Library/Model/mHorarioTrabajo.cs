﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ControlComida.Library.Model
{
    public class mHorarioTrabajo
    {
        [Key]
        [Column(Order = 0)]
        public int CodigoHorTrabajo { get; set; }
        public string DescripcionHorTrabajo { get; set; }
    }
}
