﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ControlComida.Library.Model
{
    public class mSolicitudDet
    {
        [Key]
        [Column(Order = 0)]
        public int NumeroSolicitud { get; set; }
        [Key]
        [Column(Order = 1)]
        public int TipoComida { get; set; }
        [Key]
        [Column(Order = 2)]
        public int AreadeSolicitud { get; set; }
        [Key]
        [Column(Order = 3)]
        public int CodigoEmpleado { get; set; }
        public int HorarioTrabajo { get; set; }
    }
}
