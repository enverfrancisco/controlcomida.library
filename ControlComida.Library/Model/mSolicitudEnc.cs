﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ControlComida.Library.Model
{
    public class mSolicitudEnc
    {
        [Key]
        [Column(Order = 0)]
        public int Numero_Solicitud { get; set; }
        [Key]
        [Column(Order = 1)]
        public int TipoComida { get; set; }
        [Key]
        [Column(Order = 2)]
        public int AreadeSolicitud { get; set; }
        public int IdComedor { get; set; }
        public string Observacion { get; set; }
        public string UsuarioAgrega { get; set; }
        public string UsuarioAutoriza { get; set; }
        public string UsuarioVerifica { get; set; }
        public System.DateTime FechaIngreso { get; set; }
        public System.DateTime FechaSolicitud { get; set; }
        //public virtual Entity.Data.tbl_Solicitud_Comida_Det Solicitud_ComidaDet { get; set; }
        public virtual ICollection<Entity.Data.tbl_Solicitud_Comida_Det> Solicitud_ComidaDet { get; set; }
    }
}
