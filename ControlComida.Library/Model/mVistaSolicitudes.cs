﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlComida.Library.Model
{
    public class mVistaSolicitudes
    {
        public int NumeroSolicitud { get; set; }
        public int TipoComida { get; set; }
        public int AreadeSolicitud { get; set; }
        public int IdComedor { get; set; }
        public string Observacion { get; set; }
        public string UsuarioAgrega { get; set; }
        public string UsuarioAutoriza { get; set; }
        public string UsuarioVerifica { get; set; }
        public System.DateTime FechaIngreso { get; set; }
        public System.DateTime FechaSolicitud { get; set; }
        public int HorarioTrabajo { get; set; }
        public int CodigoEmpleado { get; set; }
    }
}
